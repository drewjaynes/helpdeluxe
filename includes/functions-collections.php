<?php
/**
 * @package   Awesome Support FAQ
 * @author    ThemeAvenue <web@themeavenue.net>
 * @license   GPL-2.0+
 * @link      http://themeavenue.net
 * @copyright 2017 ThemeAvenue
 */

add_action( 'init', 'asfaq_collection_rewrite_rules' );
/**
 * Registers rewrite rules for collections on top of existing post type rewrites.
 *
 * @since 1.1
 *
 * @see add_rewrite_rule()
 */
function asfaq_collection_rewrite_rules() {

	$question_slug = asfaq_get_option( 'slug', 'question' );
	$base_slug     = asfaq_get_option( 'collection_slug', 'faqs' );

	// collection-slug/collection/category-slug
	add_rewrite_rule( $base_slug . '/([^/]+)?/([^/]+)?/?$', 'index.php?collection_name=$matches[1]&faq_category=$matches[2]', 'top' );

	// collection-slug/collection/question/faq-slug
	add_rewrite_rule( $base_slug . '/([^/]+)?/' . $question_slug . '?/([^/]+)?/?$', 'index.php?collection_name=$matches[1]&faq_slug=$matches[2]', 'top' );

	// Flush rewrites if the flag is set.
	if ( get_option( 'asfaq_flush_rewrites' ) ) {
		flush_rewrite_rules();

		// Delete the option until the next settings save.
		delete_option( 'asfaq_flush_rewrites' );
	}
}

add_action( 'init', 'asfaq_register_kb_viewer_menu' );
/**
 * Registers the 'Collections Viewer Top Menu' menu location.
 *
 * @since 1.1
 */
function asfaq_register_kb_viewer_menu() {
	register_nav_menu( 'asfaq-viewer-top-menu', __( 'Collections Viewer Top Menu', 'as-faq' ) );
}

/**
 * Retrieves an array of category IDs for the given collection.
 *
 * @since 1.1
 *
 * @param \WP_Post|int $collection Optional. Collection ID or WP_Post object.
 *                                 Default is the ID of the current collection.
 * @return int[] Array of category IDs, otherwise an empty array.
 */
function asfaq_get_collection_categories( $collection = null ) {
	if ( null === $collection ) {
		$collection = asfaq_get_current_collection();
	}

	if ( ! $collection = get_post( $collection ) ) {
		return array();
	}

	$categories = wp_get_object_terms( $collection->ID, 'as-faq-category' );

	if ( is_wp_error( $categories ) ) {
		$categories = array();
	}

	$categories = wp_list_pluck( $categories, 'term_id' );

	return $categories;
}

/**
 * Retrieves FAQ items for the current collection.
 *
 * @since 1.1
 *
 * @param mixed $collection Optional. Collection to retrieve FAQs for. Default is the current collection.
 * @return array List of FAQs for categories belonging to the collection. Empty array otherwise.
 */
function asfaq_get_faqs_for_collection( $collection = null ) {
	if ( null === $collection ) {
		$collection = asfaq_get_current_collection();
	}

	/**
	 * Filters the default limit for querying FAQ items by the current collection's categories.
	 *
	 * @since 1.1
	 *
	 * @param int   $limit      Number of FAQ items to limit queries to. Default 500 (maximum).
	 * @param mixed $collection Collection to retrieve FAQ items for.
	 */
	$limit = apply_filters( 'asfaq_faqs_for_collection_limit', 500, $collection );

	$faqs = asfaq_get_faqs( array(
		'cache_results'  => true,
		'posts_per_page' => $limit,
		'tax_query'      => array(
			'taxonomy' => 'as-faq-category',
			'fields'   => 'term_id',
			'terms'    => asfaq_get_collection_categories( $collection )
		),
	) );

	return $faqs;
}

/**
 * Retrieves the FAQ category link for a collection.
 *
 * @since 1.1
 *
 * @param \WP_Term|int|string $category   FAQ category.
 * @param int                 $collection Specific collection to tailor the category link to.
 *                                        Default will attempt to use the current collection
 *                                        if available.
 * @return string Category link.
 */
function asfaq_get_category_link_for_collection( $category, $collection = null ) {
	$category_link = '';

	if ( ! is_object( $category ) ) {
		if ( is_int( $category ) ) {
			$category = get_term( $category, 'as-faq-category' );
		} else {
			$category = get_term_by( 'slug', $category, 'as-faq-category' );
		}
	}

	if ( is_wp_error( $category ) ) {
		return $category_link;
	}

	if ( null === $collection ) {
		$collection = asfaq_get_current_collection();
	}

	$collection = get_post( $collection );

	if ( ! is_object( $category ) || ! $collection ) {
		return $category_link;
	}

	if ( 'as-faq-collection' !== get_post_type( $collection ) ) {
		return $category_link;
	}

	$base = asfaq_get_option( 'collection_slug', 'faqs' );

	$collection_slug = $collection->post_name;
	$category_slug   = $category->slug;

	$category_link = home_url( "/{$base}/{$collection_slug}/{$category_slug}/" );

	return $category_link;
}

/**
 * Retrieves the FAQ item link for a collection.
 *
 * @since 1.1
 *
 * @param \WP_Post|int|string $faq FAQ item, post_name, or WP_Post object.
 * @param null                $collection Optional. Specific collection to tailor the faq link to.
 *                                        Default will attempt to use the current collection if available.
 *
 * @return string FAQ link.
 */
function asfaq_get_faq_link_for_collection( $faq, $collection = null ) {

	$faq_link = '';

	if ( ! is_object( $faq ) ) {
		if ( is_int( $faq ) ) {
			$faq = get_post( $faq );
		} else {
			$faq = asfaq_get_faq_by_slug( $faq );
		}
	}

	if ( null === $collection ) {
		$collection = asfaq_get_current_collection();
	}

	$collection = get_post( $collection );

	if ( ! is_object( $faq ) || ! $collection ) {
		return $faq_link;
	}

	$base            = asfaq_get_option( 'collection_slug', 'faqs' );
	$question_base   = asfaq_get_option( 'slug' );
	$collection_slug = $collection->post_name;

	return home_url( "/{$base}/{$collection_slug}/{$question_base}/{$faq->post_name}" );
}

/**
 * Retrieves a Collection post object by slug.
 *
 * @since 1.1
 *
 * @param string $collection_name Collection slug.
 * @return \WP_Post|false Collection post object, otherwise false.
 */
function asfaq_get_collection_by_slug( $collection_name ) {
	$collection = get_posts( array(
		'posts_per_page' => 1,
		'post_type'      => 'as-faq-collection',
		'post_status'    => 'publish',
		'name'           => $collection_name,
	) );

	if ( ! empty( $collection[0] ) ) {
		return $collection[0];
	}

	return false;
}

/**
 * Retrieves the ID for the current collection, if available.
 *
 * This helper is intended only for use on the front-end, accessed within template files.
 *
 * @since 1.1
 *
 * @return int Current collection ID or 0.
 */
function asfaq_get_current_collection() {
	return get_query_var( 'collection', 0 );
}

/**
 * Retrieves the ID for the current collection's category, if available.
 *
 * This helper is intended only for use on the front-end, accessed within template files.
 *
 * @since 1.1
 *
 * @return int Current collection category ID or 0.
 */
function asfaq_get_current_collection_category() {
	return get_query_var( 'faq_category', 0 );
}

/**
 * Retrieves the ID for the current FAQ item, if available.
 *
 * This helper is intended only for use on the front-end, accessed within template files.
 *
 * @since 1.1
 *
 * @return int Current question (FAQ) ID or 0.
 */
function asfaq_get_current_collection_faq() {
	return get_query_var( 'faq_item', 0 );
}

/**
 * Sanitizes a collection layout for use as an HTML class.
 *
 * @since 1.1
 *
 * @param string $layout Collection layout slug.
 * @return string Sanitized layout slug.
 */
function asfaq_sanitize_collection_layout_for_class( $layout ) {
	$layout = str_replace( '.', '-', $layout );

	return sanitize_html_class( $layout );
}
