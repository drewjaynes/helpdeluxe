<?php
/**
 * @package   Awesome Support FAQ
 * @author    ThemeAvenue <web@themeavenue.net>
 * @license   GPL-2.0+
 * @link      http://themeavenue.net
 * @copyright 2014 ThemeAvenue
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

add_action( 'plugins_loaded', 'asfaq_do_action', 11 );
/**
 * Run plugin specific actions if something is specified in the GET variables
 *
 * @since 1.0
 * @return void
 */
function asfaq_do_action() {

	if ( isset( $_GET['asfaq_do'] ) ) {

		$action = filter_input( INPUT_GET, 'asfaq_do', FILTER_SANITIZE_STRING );

		if ( ! empty( $action ) ) {

			$args = $_GET;
			unset( $args['asfaq_do'] );

			$args = array_map( 'sanitize_text_field', $args );

			do_action( "asfaq_do_$action", $args );

		}

	}

}

add_action( 'admin_notices', 'asfaq_notify_new_faq' );
/**
 * Display an admin notice when a new FAQ is created during the reply submission process
 *
 * @since 1.0
 * @return void
 */
function asfaq_notify_new_faq() {

	$notice = wpas_get_notification( 'asfaq_new_faq_added', '', 'asfaq' );

	if ( ! empty( $notice ) ) {

		printf( '<div class="%s"><p>%s</p></div>', 'updated asfaq_new_faq_added', $notice );

		wpas_clean_notifications( 'asfaq' );

	}

}

add_action( 'admin_enqueue_scripts', 'asfaq_enqueue_resources_admin' );
/**
 * Load admin resources
 *
 * @since 1.0
 * @return void
 */
function asfaq_enqueue_resources_admin() {

	wp_register_style( 'asfaq-quick-links', AS_FAQ_URL . 'assets/css/admin/faq-quick-links.css', array(), AS_FAQ_VERSION, 'all' );
	wp_register_script( 'asfaq-quick-links', AS_FAQ_URL . 'assets/js/admin/faq-quick-links.js', array( 'jquery' ), AS_FAQ_VERSION, true );

	wp_enqueue_style( 'asfaq-admin', AS_FAQ_URL . 'assets/css/admin/faq-admin.css', array(), AS_FAQ_VERSION, 'all' );
	wp_enqueue_script( 'asfaq-admin-js', AS_FAQ_URL . 'assets/js/admin/faq.js', array( 'jquery' ), AS_FAQ_VERSION );

	$post = get_post();

	if ( $post && 'ticket' === $post->post_type ) {
		wp_enqueue_script( 'asfaq-quick-links' );
		wp_enqueue_style( 'asfaq-quick-links' );
	}

}

add_action( 'wp_enqueue_scripts', 'asfaq_enqueue_styles' );
/**
 * Register and load addon styles
 *
 * @since 1.0
 * @return void
 */
function asfaq_enqueue_styles() {

	wp_register_style( 'asfaq-main', AS_FAQ_URL . 'assets/css/faq.css', array(), AS_FAQ_VERSION, 'all' );
	wp_register_style( 'asfaq-collections', AS_FAQ_URL . 'assets/css/collections.css', array( 'asfaq-main' ), AS_FAQ_VERSION, 'all' );

	if ( ! is_admin() ) {
		wp_enqueue_style( 'asfaq-main' );
	}

}

add_action( 'wp_enqueue_scripts', 'asfaq_enqueue_scripts' );
/**
 * Register and load addon scripts
 *
 * @since 1.0
 * @return void
 */
function asfaq_enqueue_scripts() {

	wp_register_script( 'asfaq-main', AS_FAQ_URL . 'assets/js/faq.js', array( 'jquery' ), AS_FAQ_VERSION, true );
	wp_register_script( 'asfaq-live-search', AS_FAQ_URL . 'assets/js/faq-live-search.js', array( 'jquery' ), AS_FAQ_VERSION, true );
	wp_localize_script( 'asfaq-live-search', 'asfaq', array(
		'ajaxurl'  => admin_url( 'admin-ajax.php' ),
		'settings' => array(
			'selectors'   => asfaq_get_selectors(),
			'delay'       => (int) asfaq_get_option( 'delay', 300 ),
			'chars_min'   => (int) asfaq_get_option( 'chars_min', 3 ),
			'link_target' => asfaq_get_option( 'link_target', '_self' ),
		)
	) );

	if ( ! is_admin() ) {
		wp_enqueue_script( 'asfaq-main' );
		wp_enqueue_script( 'asfaq-live-search' );
	}

}

add_action( 'wp_head', 'asfaq_collection_header_styles' );
/**
 * Outputs dynamic collection styles in the header.
 *
 * @since 1.1
 */
function asfaq_collection_header_styles() {
	$collection = asfaq_get_current_collection();

	if ( ! $collection ) {
		return;
	}

	$titan  = TitanFramework::getInstance( 'asfaq_collections' );
	$layout = $titan->getOption( 'layout', $collection );

	// Sanitize the layout as an HTML class.
	$layout = asfaq_sanitize_collection_layout_for_class( $layout );
	$base   = '.as-faq-collection-template-' . esc_js( $layout );

	$main_bg_container  = asfaq_get_option( 'main_bg_container', '' );
	$main_bg_color      = $titan->getOption( 'main_bg_color', $collection );
	$secondary_bg_color = $titan->getOption( 'secondary_bg_color', $collection );
	$tertiary_bg_color  = $titan->getOption( 'tertiary_bg_color', $collection );

	$viewer_bg_color     = $titan->getOption( 'viewer_bg_color', $collection );
	$viewer_header       = $titan->getOption( 'viewer_header_bg_color', $collection );
	$viewer_header_menu  = $titan->getOption( 'viewer_menu_bg_color', $collection );
	$viewer_nav_color    = $titan->getOption( 'viewer_nav_bg_color', $collection );
	$viewer_nav_II_color = $titan->getOption( 'viewer_nav_highlight_bg_color', $collection );
	$viewer_nav_link     = $titan->getOption( 'viewer_nav_link_color', $collection );
	?>
	<style type="text/css">
		<?php echo esc_js( $base ); ?>,
		.collection-item .faq-count {
			background: <?php printf( '%s;', esc_attr( $main_bg_color ) ); ?>
			margin-top: 15px;
		}

		#asfaq-kb-main {
			background: <?php printf( '%s;', esc_attr( $viewer_bg_color ) ); ?>
			margin-top: 15px;
		}

		.asfaq-kb.asfaq-collection-header {
			background: <?php printf( '%s', esc_attr( $viewer_header ) ); ?>;
		}

		.asfaq-kb.asfaq-collection-header h1 {
			color: <?php printf( '%s', esc_attr( $viewer_nav_link ) ); ?>;
			margin-left: 15px;
		}

		.asfaq-viewer-top-menu {
			display: inline-block;
		}

		.asfaq-viewer-top-menu li a {
			color: <?php printf( '%s;', esc_attr( $viewer_nav_link ) ); ?>
		}

		<?php
		// e.g. .as-faq-collection-template-$layout .site { background: #fff; }
		if ( ! empty( $main_bg_container ) ) {
			printf( '%1$s %2$s { background: %3$s; }',
				esc_js( $base ),
				esc_js( $main_bg_container ),
				esc_attr( $main_bg_color )
			);
		}
		?>

		.asfaq-kb-header-menu {
			background: <?php printf( '%s;', esc_attr( $viewer_header_menu ) ); ?>
		}

		.asfaq-collection-content {
			background: <?php printf( '%s;', esc_attr( $secondary_bg_color ) ); ?>
		}

		.collection-item {
			background: <?php printf( '%s;', esc_attr( $viewer_nav_II_color ) ); ?>
		}

		ul.asfaq-kb-sidebar > li {
			background: <?php printf( '%s;', esc_attr( $viewer_nav_color ) ); ?>
		}

		ul.asfaq-kb-sidebar > li > ul,
		ul.asfaq-kb-sidebar > li > ul > li {
			background: <?php printf( '%s;', esc_attr( $tertiary_bg_color ) ); ?>
		}

		<?php echo esc_js( $base ); ?> a {
			color: <?php printf( '%s;', esc_attr( $titan->getOption( 'link_color', $collection ) ) ); ?>
		}

		<?php echo esc_js( $base ); ?> a:hover {
			color: <?php printf( '%s;', esc_attr( $titan->getOption( 'link_hover_color', $collection ) ) ); ?>
		}
	</style>
	<?php
	wp_enqueue_style( 'asfaq-collections' );
}

/**
 * Get the live search form elements selectors
 *
 * @since 1.0
 * @return array
 */
function asfaq_get_selectors() {

	$selectors = asfaq_get_option( 'selectors', '' );
	$selectors = array_filter( explode( ',', $selectors ) );

	return array_map( 'trim', $selectors );

}

/**
 * Sorts a list of objects, based on one or more orderby arguments.
 *
 * Serves as back-compat shim for wp_list_sort() in pre-WP 4.7.0 installs.
 *
 * @since 1.1
 *
 * @param array        $list          An array of objects to filter.
 * @param string|array $orderby       Optional. Either the field name to order by or an array
 *                                    of multiple orderby fields as $orderby => $order.
 * @param string       $order         Optional. Either 'ASC' or 'DESC'. Only used if $orderby
 *                                    is a string.
 * @param bool         $preserve_keys Optional. Whether to preserve keys. Default false.
 * @return array The sorted array.
 */
function asfaq_list_sort( $list, $orderby = array(), $order = 'ASC', $preserve_keys = false ) {

	if ( function_exists( 'wp_list_sort' ) ) {

		$list = wp_list_sort( $list, $orderby, $order, true );

	} else {

		if ( empty( $orderby ) ) {
			return $list;
		}

		if ( is_string( $orderby ) ) {
			$orderby = array( $orderby => $order );
		}

		foreach ( $orderby as $field => $direction ) {
			$orderby[ $field ] = 'DESC' === strtoupper( $direction ) ? 'DESC' : 'ASC';
		}

		if ( true === $preserve_keys ) {

			uasort( $list, function( $a, $b ) use ( $orderby ) {
				$a = $a->to_array();
				$b = $b->to_array();

				foreach ( $orderby as $field => $direction ) {
					if ( ! isset( $a[ $field ] ) || ! isset( $b[ $field ] ) ) {
						continue;
					}

					if ( $a[ $field ] == $b[ $field ] ) {
						continue;
					}

					$results = 'DESC' === $direction ? array( 1, -1 ) : array( -1, 1 );

					if ( is_numeric( $a[ $field ] ) && is_numeric( $b[ $field ] ) ) {
						return ( $a[ $field ] < $b[ $field ] ) ? $results[0] : $results[1];
					}

					return 0 > strcmp( $a[ $field ], $b[ $field ] ) ? $results[0] : $results[1];
				}

				return 0;

			} );

		} else {

			/*
			 * This is basically verbatim code duplication with the uasort() closure used above, but
			 * by using a closure. we're able to inherit $orderby via the use keyword without having
			 * to create an even greater abstraction later, e.g. a list sorting class with properties
			 * for back-compat with the core WP_List_Util class.
			 */
			usort( $list, function( $a, $b ) use ( $orderby ) {
				$a = $a->to_array();
				$b = $b->to_array();

				foreach ( $orderby as $field => $direction ) {
					if ( ! isset( $a[ $field ] ) || ! isset( $b[ $field ] ) ) {
						continue;
					}

					if ( $a[ $field ] == $b[ $field ] ) {
						continue;
					}

					$results = 'DESC' === $direction ? array( 1, -1 ) : array( -1, 1 );

					if ( is_numeric( $a[ $field ] ) && is_numeric( $b[ $field ] ) ) {
						return ( $a[ $field ] < $b[ $field ] ) ? $results[0] : $results[1];
					}

					return 0 > strcmp( $a[ $field ], $b[ $field ] ) ? $results[0] : $results[1];
				}

				return 0;

			} );

		}

	}

	return $list;

}

/**
 * List all site pages.
 *
 * @since 1.1
 *
 * @param string $post_type Optional. Post type to list items for. Default 'page'.
 * @return array List of pages.
 */
function asfaq_list_pages( $post_type = 'page' ) {

	$list = array( '' => __( 'None', 'as-faq' ) );

	$pages = get_posts( array(
		'post_type'              => $post_type,
		'post_status'            => 'publish',
		'order'                  => 'DESC',
		'orderby'                => 'page_title',
		'posts_per_page'         => 500,
		'no_found_rows'          => true,
		'cache_results'          => false,
		'update_post_term_cache' => false,
		'update_post_meta_cache' => false,
	) );

	if ( ! empty( $pages ) ) {

		foreach ( $pages as $page ) {
			$list[ $page->ID ] = $page->post_title;
		}

	}

	/**
	 * Filters the ASFAQ pages list.
	 *
	 * @since 1.1
	 *
	 * @param array  $list      List of pages.
	 * @param string $post_type Post type.
	 */
	return apply_filters( 'asfaq_list_pages', $list, $post_type );
}
