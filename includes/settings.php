<?php
/**
 * @package   Awesome Support FAQ/Settings
 * @author    ThemeAvenue <web@themeavenue.net>
 * @license   GPL-2.0+
 * @link      http://themeavenue.net
 * @copyright 2014 ThemeAvenue
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

add_filter( 'asfaq_plugin_settings', 'asfaq_settings' );
/**
 * Register plugin settings
 *
 * @since 1.0
 *
 * @param array $def Plugin settings
 *
 * @return array
 */
function asfaq_settings( $def ) {

	$settings = array(
		'general' => array(
			'name'    => __( 'General', 'as-faq' ),
			'options' => array(
				array(
					'name'    => __( 'Reply &amp; FAQ Closes', 'as-faq' ),
					'id'      => 'reply_faq_close',
					'type'    => 'checkbox',
					'desc'    => __( 'Close tickets when replied using the <em>Reply &amp; FAQ</em> button.', 'as-faq' ),
					'default' => false
				),
				array(
					'name'    => __( 'Quick FAQ Links Template', 'as-faq' ),
					'id'      => 'quick_reply_template',
					'type'    => 'editor',
					'desc'    => sprintf( __( 'Reply to send to the client for directing him to the FAQ article. <a %s>Click here</a> to review all available template tags.', 'as-faq' ), 'href="#contextual-help-link" onclick="document.getElementById(\'contextual-help-link\').click(); return false;"' ),
					'default' => '<p>Hey {client_name},</p><p>This question has been answered in our FAQ. Please check out the answer here: {faq_link}.</p><p>I believe this will help you solve the problem. However, if you need further assistance, feel free to get back to me.</p><p>Cheers,<br>{agent_name}</p>',
				),
				array(
					'name'    => __( 'Rewrite Slug', 'as-faq' ),
					'id'      => 'slug',
					'type'    => 'text',
					'desc'    => sprintf( __( 'What should the slug be for FAQs? The slug is the part that prefixes the question slug.<br />Example: %s', 'as-faq' ), '<code>http://domain.com/<strong><span class="rewrite_slug_preview">' . asfaq_get_option( 'slug', 'question' ) . '</span></strong>/my-question</code>' ),
					'default' => 'question'
				),
				array(
					'name'    => __( 'Collection Rewrite Slug', 'as-faq' ),
					'id'      => 'collection_slug',
					'type'    => 'text',
					'desc'    => sprintf( __( 'What should the slug be for FAQ collections? The slug is the part that prefixes the collection slug.<br />Example: %s', 'as-faq' ), '<code>http://domain.com/<strong><span class="collection_rewrite_slug_preview">' . asfaq_get_option( 'collection_slug', 'help' ) . '</span></strong>/my-collection</code>' ),
					'default' => 'help'
				),
			)
		),
		'live_search' => array(
			'name'    => __( 'Live Search', 'as-faq' ),
			'options' => array(
				array(
					'name'    => __( 'Live Search', 'as-faq' ),
					'id'      => 'selectors',
					'type'    => 'text',
					'desc'    => sprintf( __( 'On which elements should the live search trigger? By default, it is enabled on the ticket submission form title field. You can add more form elements by specifying their selector. If you use multiple selectors, they must be separated by a comma (%s). <a %s>Read more about selectors</a>.', 'as-faq' ), '<code>,</code>', 'href="http://www.w3schools.com/jquery/jquery_selectors.asp" target="_blank"' ),
					'default' => '#wpas_title'
				),
				array(
					'name'    => __( 'Delay', 'as-faq' ),
					'id'      => 'delay',
					'type'    => 'text',
					'desc'    => __( 'Delay (in <code>milliseconds</code>) after which the live search is triggered when the user types something.', 'as-faq' ),
					'default' => 300
				),
				array(
					'name'    => __( 'Characters Min.', 'as-faq' ),
					'id'      => 'chars_min',
					'type'    => 'number',
					'desc'    => __( 'Minimum number of characters required to trigger the live search.', 'as-faq' ),
					'default' => 3,
					'max'     => 10,
				),
				array(
					'name'    => __( 'Link Target', 'as-faq' ),
					'id'      => 'link_target',
					'type'    => 'select',
					'desc'    => __( 'Where do you want links to open?', 'as-faq' ),
					'options' => array( '_blank' => esc_html__( 'New window/tab', 'as-faq' ), '_self' => esc_html__( 'Same window/tab', 'as-faq' ) ),
					'default' => '_self'
				),
				array(
					'name'    => __( 'Sort Results', 'as-faq' ),
					'id'      => 'sort_results',
					'type'    => 'select',
					'desc'    => __( 'How do you want live search results ot be displayed?', 'as-faq' ),
					'options' => array(
						'date_asc'   => esc_html__( 'Date (ascending)', 'as-faq' ),
						'date_desc'  => esc_html__( 'Date (descending)', 'as-faq' ),
						'title_asc'  => esc_html__( 'Title (ascending)', 'as-faq' ),
						'title_desc' => esc_html__( 'Date (descending)', 'as-faq' ),
					),
					'default' => 'date_desc'
				),
				array(
					'name'    => __( 'Max. Results', 'as-faq' ),
					'id'      => 'display_max',
					'type'    => 'number',
					'desc'    => __( 'Maximum number of results to display.', 'as-faq' ),
					'default' => 5,
					'max'     => 20,
				),
				
				array(
					'name'    => __( 'Live Search Results Styles', 'as-faq' ),
					'type'    => 'heading',
					'desc'    => __( 'These options control the look of the live search results', 'as-faq' ),
				),
				array(
					'name'    => __( 'Section Background Color', 'as-faq' ),
					'id'      => 'faq-live-search-section-background-color',
					'type'    => 'color',
					'desc'    => __( 'Color of the background on which the live search results are shown', 'as-faq' ),
					'default' => '#64CA92'
				),
				array(
					'name'    => __( 'Topic Title Color', 'as-faq' ),
					'id'      => 'faq-live-search-topic-title-color',
					'type'    => 'color',
					'desc'    => __( 'Color of the topics / results that are shown', 'as-faq' ),
					'default' => '#ffffff'
				),				
			)
		),
		'collections' => array(
			'name'    => __( 'Collections', 'as-faq' ),
			'options' => array(
				array(
					'name'    => __( 'FAQ Item Counts (default)', 'as-faq' ),
					'id'      => 'default_item_counts',
					'type'    => 'checkbox',
					'default' => false,
					'desc'    => __( 'Default setting for whether to display FAQ item counts next to categories.', 'as-faq' ),
				),
				array(
					'name'    => __( 'FAQ List Style (default)', 'as-faq' ),
					'id'      => 'default_list_style',
					'type'    => 'radio',
					'default' => 'accordion',
					'desc'    => __( 'Default setting for how to list FAQ items in supported layouts.', 'as-faq' ),
					'options' => array(
						'accordion'     => esc_html__( 'Accordion', 'as-faq' ),
						'links'         => esc_html__( 'Links', 'as-faq' ),
						'links_bullets' => esc_html__( 'Bulleted Links', 'as-faq' ),
					),
				),
				array(
					'name'    => __( 'Show Collection Title (default', 'as-faq' ),
					'id'      => 'default_show_title',
					'type'    => 'checkbox',
					'desc'    => __( 'If checked, the collection title will be shown by default.', 'as-faq' ),
					'default' => true,
				),
				array(
					'name'    => __( 'Main Container' ),
					'id'      => 'main_bg_container',
					'type'    => 'text',
					'desc'    => __( 'Specify a CSS class or ID to use for targeting the theme&#8217;s "main" container.<br />Be sure to include the leading period or hash to signify a CSS class or ID, e.g. #main, or .site.', 'as-faq' ),
					'default' => '.site'
				),
				array(
					'name'    => __( 'Main Background Color (default)' ),
					'id'      => 'default_main_bg_color',
					'type'    => 'color',
					'default' => '#FFFFFF',
					'desc'    => __( 'Choose a default main background color for all collections.', 'as-faq' )
				),
				array(
					'name'    => __( 'Secondary Background Color (default)' ),
					'id'      => 'default_secondary_bg_color',
					'type'    => 'color',
					'default' => '#5bbdbf',
					'desc'    => __( 'Choose a default secondary background color for all collections.', 'as-faq' )
				),
				array(
					'name'    => __( 'Tertiary Background Color (default)' ),
					'id'      => 'default_tertiary_bg_color',
					'type'    => 'color',
					'default' => '#D6D6D6',
					'desc'    => __( 'Choose a default tertiary background color for all collections.', 'as-faq' )
				),
				array(
					'name'    => __( 'Link Color (default)' ),
					'id'      => 'default_link_color',
					'type'    => 'color',
					'default' => '#000000',
					'desc'    => __( 'Choose a color for all collections links.', 'as-faq' )
				),
				array(
					'name'    => __( 'Viewer Background Color (default)', 'as-faq' ),
					'id'      => 'default_viewer_bg_color',
					'type'    => 'color',
					'desc'    => __( 'Choose a background color for the knowledge base viewer.', 'as-faq' ),
					'default' => '#FFFFFF',
				),
				array(
					'name'    => __( 'Viewer Link Color (default)', 'as-faq' ),
					'id'      => 'default_viwer_link_color',
					'type'    => 'color',
					'desc'    => __( 'Choose a hover color for links in the knowledge viewer.', 'as-faq' ),
					'default' => '#000000',
				),
				array(
					'name'    => __( 'Viewer Header Background Color (default)', 'as-faq' ),
					'id'      => 'default_viewer_header_bg_color',
					'type'    => 'color',
					'desc'    => __( 'Choose a background color the knowledge base header bar.', 'as-faq' ),
					'default' => '#000000',
				),
				array(
					'name'    => __( 'Viewer Header Menu Background Color (default)', 'as-faq' ),
					'id'      => 'default_viewer_menu_bg_color',
					'type'    => 'color',
					'desc'    => __( 'Choose a background color the knowledge base viewer menu bar.', 'as-faq' ),
					'default' => '#000000',
				),
				array(
					'name'    => __( 'Viewer Nav Background Color (default)', 'as-faq' ),
					'id'      => 'default_viewer_nav_bg_color',
					'type'    => 'color',
					'desc'    => __( 'Choose a background color for the knowledge base viewer navigation.', 'as-faq' ),
					'default' => '#5bbdbf',
				),
				array(
					'name'    => __( 'Viewer Nav Highlight Color (default)', 'as-faq' ),
					'id'      => 'default_viewer_nav_highlight_bg_color',
					'type'    => 'color',
					'desc'    => __( 'Choose a highlight background color for the knowledge base viewer navigation.', 'as-faq' ),
					'default' => '#D6D6D6',
				),
				array(
					'name'    => __( 'Viewer Nav Link Color (default)', 'as-faq' ),
					'id'      => 'default_viewer_nav_link_color',
					'type'    => 'color',
					'desc'    => __( 'Choose a color for the knowledge base navigation links.', 'as-faq' ),
					'default' => '#FFFFFF',
				),

			)
		),
	);

	return array_merge( $def, $settings );

}