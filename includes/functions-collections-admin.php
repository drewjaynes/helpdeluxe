<?php
/**
 * @package   Awesome Support FAQ
 * @author    ThemeAvenue <web@themeavenue.net>
 * @license   GPL-2.0+
 * @link      http://themeavenue.net
 * @copyright 2017 ThemeAvenue
 */

add_action( 'tf_create_options', 'asfaq_collection_admin_settings_meta_box' );
/**
 * Registers the settings meta box for the current collection with the Titan Framework.
 *
 * @since 1.1
 */
function asfaq_collection_admin_settings_meta_box() {
	$collection = get_the_ID();
	$titan      = TitanFramework::getInstance( 'asfaq_collections' );

	$meta_box = $titan->createMetaBox( array(
			'name'      => esc_html__( 'Display Settings', 'as-faq' ),
			'id'        => "asfaq-{$collection}-settings",
			'post_type' => 'as-faq-collection',
		)
	);

	$viewer_meta_box = $titan->createMetaBox( array(
		'name'      => esc_html__( 'Knowledge Base Viewer Display Settings', 'as-faq' ),
		'id'        => "asfaq-{$collection}-viewer-settings",
		'post_type' => 'as-faq-collection',
	) );

	/**
	 * Filters the default collection settings.
	 *
	 * @since 1.1
	 *
	 * @param array $settings Default collection settings.
	 */
	$options = apply_filters( 'asfaq_collection_default_settings', array() );

	foreach ( $options as $option ) {
		if ( isset( $option['id'] ) && 'viewer' === substr( $option['id'], 0, 6 ) ) {
			$viewer_meta_box->createOption( $option );
		} else {
			$meta_box->createOption( $option );
		}
	}

}

add_action( 'save_post', 'asfaq_save_collection_page_template', 11 );
/**
 * Saves the collection page template after Titan meta has been saved.
 *
 * @since 1.1
 *
 * @param int $collection_id Collection ID.
 */
function asfaq_save_collection_page_template( $collection_id ) {
	$titan  = TitanFramework::getInstance( 'asfaq_collections' );
	$layout = $titan->getOption( 'layout', $collection_id );

	update_post_meta( $collection_id, '_wp_page_template', $layout );
}

add_action( 'add_meta_boxes', 'asfaq_collection_meta_boxes' );
/**
 * Registers meta boxes to be displayed with the Collections editor.
 *
 * @since 1.1
 *
 * @see add_meta_box()
 */
function asfaq_collection_meta_boxes() {
	add_meta_box( 'collection-checklist', __( 'Collection Checklist', 'as-faq' ), 'asfaq_collection_checklist_cb', 'as-faq-collection', 'normal', 'high' );
}

/**
 * Renders the contents of the Collection Checklist meta box in the Collection editor.
 *
 * @since 1.1
 *
 * @param \WP_Post $collection Current collection object.
 */
function asfaq_collection_checklist_cb( $collection ) {
	$titan = TitanFramework::getInstance( 'asfaq_collections' );

	$layout     = $titan->getOption( 'layout', $collection->ID );
	$list_style = $titan->getOption( 'list_style', $collection->ID );
	$categories = wp_get_object_terms( $collection->ID, 'as-faq-category' );
	?>
	<div id="asfaq-collection-checklist">
		<table class="asfaq-collection-checklist">
			<tbody>
			<tr class="<?php echo esc_attr( $layout ) ? 'has-item' : 'needs-item'; ?>">
				<td><i class="dashicons dashicons-<?php echo esc_attr( $layout ) ? 'yes' : 'no'; ?>"></i></td>
				<td><?php _e( 'Select a layout for this collection.', 'as-faq' ); ?></td>
			</tr>
			<tr class="<?php echo ! empty( $categories ) ? 'has-item' : 'needs-item'; ?>">
				<td><i class="dashicons dashicons-<?php echo ! empty( $categories ) ? 'yes' : 'no'; ?>"></i></td>
				<td><?php _e( 'Associate this collection with one or more categories.', 'as-faq' ); ?></td>
			</tr>
			<tr class="<?php echo $list_style ? 'has-item' : 'needs-item'; ?>">
				<td><i class="dashicons dashicons-<?php echo esc_attr( $list_style ) ? 'yes' : 'no'; ?>"></i></td>
				<td><?php _e( 'Configure collection display settings.', 'as-faq' ); ?></td>
			</tr>
			</tbody>
		</table>
	</div>
	<?php
}

add_filter( 'asfaq_collection_default_settings', 'asfaq_collection_settings', 11 );
/**
 * Registers default collection settings.
 *
 * @since 1.1
 *
 * @param array $default_settings Default collection settings.
 * @return array Filtered collection settings defaults.
 */
function asfaq_collection_settings( $default_settings ) {

	// Global settings.
	$titan = TitanFramework::getInstance( 'asfaq' );

	$settings = array(
		array(
			'name'    => __( 'Layout', 'as-faq' ),
			'id'      => 'layout',
			'type'    => 'select',
			'desc'    => __( 'Choose a layout to use for this collection.', 'as-faq' ),
			'default' => 'asfaq-collection-knowledge-base.php',
			'options' => array(
				'asfaq-collection-category-boxes.php' => esc_html__( 'Category Boxes', 'as-faq' ),
				'asfaq-collection-category-lists.php' => esc_html__( 'Category Lists', 'as-faq' ),
				'asfaq-collection-columns.php'        => esc_html__( 'Columns', 'as-faq' ),
				'asfaq-collection-knowledge-base.php' => esc_html__( 'Knowledge Base', 'as-faq' ),
			),
		),
		array(
			'name'    => __( 'FAQ Item Counts', 'as-faq' ),
			'id'      => 'item_counts',
			'type'    => 'checkbox',
			'desc'    => __( 'Display FAQ item counts next to categories.', 'as-faq' ),
			'default' => $titan->getOption( 'default_item_counts' ), // false
		),
		array(
			'name'    => __( 'FAQ List Style', 'as-faq' ),
			'id'      => 'list_style',
			'type'    => 'radio',
			'desc'    => __( 'How to list FAQ items in supported layouts. In some layouts, this setting may only apply in sub-views.', 'as-faq' ),
			'options' => array(
				'accordion'     => esc_html__( 'Accordion', 'as-faq' ),
				'links'         => esc_html__( 'Links', 'as-faq' ),
				'links_bullets' => esc_html__( 'Bulleted Links', 'as-faq' ),
			),
			'default' => $titan->getOption( 'default_list_style' ), // accordion
		),
		array(
			'name' => __( 'Show Collection Title', 'as-faq' ),
			'id'   => 'show_title',
			'type' => 'checkbox',
			'desc' => __( 'If checked, the collection title will be displayed.', 'as-faq' ),
			'default' => $titan->getOption( 'default_hide_title' ),
		),
		array(
			'name'    => __( 'Main Background Color', 'as-faq' ),
			'id'      => 'main_bg_color',
			'type'    => 'color',
			'desc'    => __( 'Choose a primary background color for this collection.', 'as-faq' ),
			'default' => $titan->getOption( 'default_main_bg_color' ), // #FFFFFF
		),
		array(
			'name'    => __( 'Secondary Background Color', 'as-faq' ),
			'id'      => 'secondary_bg_color',
			'type'    => 'color',
			'desc'    => __( 'Choose a secondary background color for this collection.', 'as-faq' ),
			'default' => $titan->getOption( 'default_secondary_bg_color' ), // #F4F4F4
		),
		array(
			'name'    => __( 'Tertiary Background Color', 'as-faq' ),
			'id'      => 'tertiary_bg_color',
			'type'    => 'color',
			'desc'    => __( 'Choose a tertiary background color for this collection.', 'as-faq' ),
			'default' => $titan->getOption( 'default_tertiary_bg_color' ), // #FFFFFF
		),
		array(
			'name'    => __( 'Link Color', 'as-faq' ),
			'id'      => 'link_color',
			'type'    => 'color',
			'desc'    => __( 'Choose a color for links in this collection.', 'as-faq' ),
			'default' => $titan->getOption( 'default_link_color' ), // #000000
		),
	);

	return array_merge( $default_settings, $settings );

}

add_filter( 'asfaq_collection_default_settings', 'asfaq_collection_viewer_settings', 12 );
/**
 * Registers default collection viewer display settings.
 *
 * @since 1.1
 *
 * @param array $default_settings Default collection settings.
 * @return array Filtered collection settings defaults.
 */
function asfaq_collection_viewer_settings( $default_settings ) {

	// Global settings.
	$titan = TitanFramework::getInstance( 'asfaq' );

	// Viewer setting defaults fall back to the collection setting first, then the global default.
	$settings = array(
		array(
			'name'    => __( 'Background Color', 'as-faq' ),
			'id'      => 'viewer_bg_color',
			'type'    => 'color',
			'desc'    => __( 'Choose a background color for the knowledge base viewer.', 'as-faq' ),
			'default' => $titan->getOption( 'default_viewer_bg_color' ), #FFFFFF
		),
		array(
			'name'    => __( 'Link Color', 'as-faq' ),
			'id'      => 'viewer_link_color',
			'type'    => 'color',
			'desc'    => __( 'Choose a hover color for links in the knowledge viewer.', 'as-faq' ),
			'default' => $titan->getOption( 'default_viewer_link_color' ), // #4444444
		),
		array(
			'name'    => __( 'Header Background Color', 'as-faq' ),
			'id'      => 'viewer_header_bg_color',
			'type'    => 'color',
			'desc'    => __( 'Choose a background color the knowledge base header bar.', 'as-faq' ),
			'default' => $titan->getOption( 'default_viewer_header_bg_color' ), // #000000
		),
		array(
			'name'    => __( 'Menu Background Color', 'as-faq' ),
			'id'      => 'viewer_menu_bg_color',
			'type'    => 'color',
			'desc'    => __( 'Choose a background color the knowledge base viewer menu bar.', 'as-faq' ),
			'default' => $titan->getOption( 'default_viewer_menu_bg_color' ),
		),
		array(
			'name'    => __( 'Nav Background Color', 'as-faq' ),
			'id'      => 'viewer_nav_bg_color',
			'type'    => 'color',
			'desc'    => __( 'Choose a background color for the knowledge base viewer navigation.', 'as-faq' ),
			'default' => $titan->getOption( 'default_viewer_nav_bg_color' ),
		),
		array(
			'name'    => __( 'Nav Highlight Color', 'as-faq' ),
			'id'      => 'viewer_nav_highlight_bg_color',
			'type'    => 'color',
			'desc'    => __( 'Choose a highlight background color for the knowledge base viewer navigation.', 'as-faq' ),
			'default' => $titan->getOption( 'default_viewer_nav_highlight_bg_color' ),
		),
		array(
			'name'    => __( 'Nav Link Color', 'as-faq' ),
			'id'      => 'viewer_nav_link_color',
			'type'    => 'color',
			'desc'    => __( 'Choose a color for the knowledge base navigation links.', 'as-faq' ),
			'default' => $titan->getOption( 'default_viewer_nav_link_color' ), // #000000
		),
	);

	return array_merge( $default_settings, $settings );

}
