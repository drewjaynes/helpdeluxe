<?php
/**
 * @package   Awesome Support FAQ
 * @author    ThemeAvenue <web@themeavenue.net>
 * @license   GPL-2.0+
 * @link      http://themeavenue.net
 * @copyright 2017 ThemeAvenue
 */

add_filter( 'single_template', 'asfaq_catch_collection_template' );
/**
 * "Catches" and filters collection template file paths and passes them
 * through the AS template hierarchy.
 *
 * @since 1.1
 *
 * @param string $template Template name or path.
 * @return string (Maybe) filtered template path.
 */
function asfaq_catch_collection_template( $template ) {

	$object  = get_queried_object();
	$matched = preg_match( '/^asfaq\-collection\-/', $template );

	// If not matched and this is a collection, try to get the page template directly.
	if ( ! $matched && 'as-faq-collection' === get_post_type( $object ) ) {
		$template_slug = get_page_template_slug( $object );

		if ( $template_slug ) {
			$template = $template_slug;
			$matched = true;
		}
	}

	if ( $matched ) {
		// Remove the file extension (it'll be re-added when the path is determined).
		$template = str_replace( '.php', '', $template );

		// Try to find the template.
		$template = wpas_locate_template( $template );
	}

	return $template;
}

/**
 * Builds markup for displaying FAQs for a given category.
 *
 * @since 1.1
 *
 * @param int\WP_Term $category     Category term object or ID. Default is the current category.
 * @param array       $display_args {
 *     Optional. Display arguments for the FAQs markup.
 *
 *     @type bool $show_header Whether to show the category title header in the list. Default true.
 *     @type bool $children    Whether to display child category FAQs as well as first-level items.
 *                             Default false.
 * }
 * @return string HTML markup for listing FAQs from the given category.
 */
function asfaq_get_category_faqs_markup( $category = 0, $display_args = array() ) {
	if ( empty( $category ) ) {
		$category = asfaq_get_current_collection_category();
	}

	$category   = get_term( $category );
	$collection = get_post( asfaq_get_current_collection() );

	if ( is_wp_error( $category ) || ! $collection ) {
		return '';
	}

	$args = wp_parse_args( $display_args, array(
		'show_header' => true,
		'children'    => false,
	) );

	$output = '';

	$faqs = asfaq_get_faqs( array(
		'cache_results'  => true,
		'posts_per_page' => 100,
		'tax_query'      => array(
			array(
				'taxonomy'         => 'as-faq-category',
				'fields'           => 'term_id',
				'terms'            => $category->term_id,
				'include_children' => $args['children'],
			),
		),
	) );

	if ( ! empty( $faqs ) ) {

		if ( true === $args['show_header'] ) {
			$output .= sprintf( '<h2 class="asfaq-kb-category-header">%s</h2>', esc_html( $category->name ) );
		}

		$output .= asfaq_get_faqs_list_markup( $collection, $faqs );
	}

	return $output;
}

/**
 * Builds markup for displaying FAQs for a given list style.
 *
 * @since 1.1
 *
 * @param int|\WP_Post $collection Collection ID or object.
 * @param \WP_Post[]   $faqs       FAQ post objects to list.
 * @param string       $list_style Optional. List style to use. If empty, the list style for
 *                                 the collection will be used. Accepts 'links', 'links_bullets',
 *                                 or 'accordion'. Default empty.
 */
function asfaq_get_faqs_list_markup( $collection, $faqs, $list_style = '' ) {
	$output = '';

	$collection = get_post( $collection );

	if ( ! $collection || ( $collection && 'as-faq-collection' !== get_post_type( $collection ) ) ) {
		return $output;
	}

	$titan = TitanFramework::getInstance( 'asfaq_collections' );

	if ( ! in_array( $list_style, array( 'links', 'links_bullets', 'accordion' ), true ) ) {
		$list_style = $titan->getOption( 'list_style', $collection->ID );
	}

	if ( ! empty( $faqs ) ) {

		$output .= '<div class="asfaq-collection-list collection-' . $collection->ID . '">';

		switch ( $list_style ) {
			case 'links':
				ob_start();

				foreach ( $faqs as $faq ) {
					?>
					<a href="<?php echo esc_url( asfaq_get_faq_link_for_collection( $faq, $collection ) ); ?>" class="asfaq_item_link asfaq_item_<?php esc_attr( $faq->ID ); ?>">
						<?php echo get_the_title( $faq ); ?>
					</a>
					<?php
				}

				$output .= ob_get_clean();
				break;

			case 'links_bullets':
				ob_start();
				?>

				<ul>
					<?php foreach ( $faqs as $faq ) : ?>
						<li>
							<a href="<?php echo esc_url( asfaq_get_faq_link_for_collection( $faq, $collection ) ); ?>" class="asfaq_item_link asfaq_item_<?php esc_attr( $faq->ID ); ?>">
								<?php echo get_the_title( $faq ); ?>
							</a>
						</li>
					<?php endforeach; ?>
				</ul>

				<?php
				$output .= ob_get_clean();
				break;

			case 'accordion':
			default:
				ob_start();

				foreach ( $faqs as $faq ) :
					?>
					<div class="asfaq_item asfaq_item_<?php echo esc_attr( $faq->ID ); ?>" data-id="<?php echo esc_attr( $faq->ID ); ?>">
						<h3 class="asfaq_title">
							<?php echo apply_filters( 'the_title', $faq->post_title ); ?>
						</h3>
						<div class="asfaq_answer">
							<?php echo apply_filters( 'the_content', $faq->post_content ); ?>
						</div>
					</div>
					<?php
				endforeach;

				$output .= ob_get_clean();
				break;
		}

		$output .= '</div>';
	}

	return $output;
}

add_filter( 'template_include', 'asfaq_maybe_override_template' );
/**
 * Filters the single FAQ item template to use the knowledge base viewer template.
 *
 * The single template will only be overridden if the 'single_faq_viewer_override'
 * setting is enabled.
 *
 * @since 1.1
 *
 * @param string $template Template path for a single FAQ item.
 * @return string (Maybe) filtered singular template.
 */
function asfaq_maybe_override_template( $template ) {
	if ( get_query_var( 'collection' )
		&& ( 'faq' === get_query_var( 'post_type' ) || is_tax( 'as-faq-category' ) )
	) {
		$template = wpas_locate_template( 'asfaq-collection-knowledge-base' );
	}

	return $template;
}

add_filter( 'wpas_locate_template', 'asfaq_locate_template', 100, 2 );
/**
 * Filters the template path to allow loading templates from the AS FAQ themes directory.
 *
 * @since 1.1
 *
 * @see wpas_get_template()
 *
 * @param string $template Template path (if found).
 * @param string $name     Name of the template to locate.
 * @return string (Maybe) modified template path.
 */
function asfaq_locate_template( $template, $name ) {

	$templates = array(
		'asfaq-collection-category-boxes',
		'asfaq-collection-category-lists',
		'asfaq-collection-columns',
		'asfaq-collection-knowledge-base',
	);

	if ( in_array( $name, $templates, true ) ) {
		if ( false !== strpos( $template, WPAS_PATH . 'themes' ) ) {
			$template = AS_FAQ_PATH . "themes/default/faq/{$name}.php";
		}
	}

	return $template;
}

add_action( 'pre_get_posts', 'asfaq_set_collection_request_vars' );
/**
 * Sets request variables (when present) for use by template_include callbacks
 * for loading collection templates.
 *
 * @since 1.1
 *
 * @param \WP_Query $query Query instance.
 */
function asfaq_set_collection_request_vars( $query ) {
	// Bail if in the admin.
	if ( is_admin() || ! $query->is_main_query() ) {
		return;
	}

	$collection_slug = $query->get( 'collection_name' );

	if ( ! $collection_slug ) {
		$collection_slug = $query->get( 'as-faq-collection' );
	}

	if ( $collection_slug ) {
		$collection = asfaq_get_collection_by_slug( $collection_slug );

		if ( $collection ) {
			$query->set( 'collection', $collection->ID );
		}
	}

	if ( $faq_category_slug = $query->get( 'faq_category' ) ) {
		$category = get_term_by( 'slug', $faq_category_slug, 'as-faq-category' );

		if ( $category && ! is_wp_error( $category ) ) {
			$query->set( 'faq_category', $category->term_id );
			$query->set( 'post_type', 'faq' );

			$query->is_tax = true;
			$query->is_home = false;
			$query->queried_object = $category;
			$query->queried_object_id = $category->term_id;
		}
	}

	if ( $faq_question_slug = $query->get( 'faq_slug' ) ) {
		$question = asfaq_get_faq_by_slug( $faq_question_slug );

		if ( $question ) {
			$query->set( 'faq_item', $question->ID );
			$query->set( 'post_type', 'faq' );

			$query->is_singular = true;
			$query->is_home = false;
			$query->queried_object = $question;
			$query->queried_object_id = $question->ID;
		}
	}

}

add_filter( 'query_vars', 'asfaq_register_collection_query_vars' );
/**
 * Registers query variables for use in template loading and query manipulation.
 *
 * @since 1.1
 *
 * @param array $vars List of currently whitelisted query vars.
 * @return array Modified list of whitelisted query vars.
 */
function asfaq_register_collection_query_vars( $vars ) {
	$collection_vars = array( 'faq_category', 'collection', 'faq_item', 'collection_name', 'faq_slug' );

	return array_merge( $vars, $collection_vars );
}

add_filter( 'body_class', 'asfaq_collection_body_classes' );
/**
 * Adds collection-specific body classes for sub-templates.
 *
 * @since 1.1
 *
 * @param array $classes Currently-set body classes.
 * @return array (Maybe) modified body classes.
 */
function asfaq_collection_body_classes( $classes ) {
	if ( ! $collection = asfaq_get_current_collection() ) {
		return $classes;
	}

	$titan  = TitanFramework::getInstance( 'asfaq_collections' );
	$layout = $titan->getOption( 'layout', $collection );

	$layout = asfaq_sanitize_collection_layout_for_class( $layout );

	// Add the page template body class if it isn't already there.
	if ( ! in_array( $layout, $classes, true ) ) {
		$classes[] = 'as-faq-collection-template-' . $layout;
	}

	// Add a class for the current theme (for targeting specific theme incompatibilities).
	if ( $current_theme = get_option( 'current_theme', '' ) ) {
		$classes[] = sanitize_key( $current_theme );
	}

	return $classes;
}

/**
 * Outputs a basic header for use in collection templates built to "take over" the theme.
 *
 * @since 1.1
 */
function asfaq_get_header() {
	?>
	<!DOCTYPE html>
	<html <?php language_attributes(); ?> class="no-js">
		<head>
			<meta charset="<?php bloginfo( 'charset' ); ?>">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<link rel="profile" href="http://gmpg.org/xfn/11">
			<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
				<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
			<?php endif; ?>
			<?php wp_head(); ?>
		</head>

	<body <?php body_class(); ?>>
	<?php
}

/**
 * Outputs a basic footer for use in collection templates built to "take over" the theme.
 *
 * @since 1.1
 */
function asfaq_get_footer() {
	wp_footer();
	?>
	</body>
	</html>
	<?php
}
