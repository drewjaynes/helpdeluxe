<?php
/**
 * @package   Awesome Support FAQ
 * @author    ThemeAvenue <web@themeavenue.net>
 * @license   GPL-2.0+
 * @link      http://themeavenue.net
 * @copyright 2017 ThemeAvenue
 */

/**
 * Retrieves FAQ category term objects.
 *
 * @since 1.1
 *
 * @param array $args Arguments to retrieve FAQ categories. See {@see get_terms()}.
 *
 * @return array|int|WP_Error Array of terms, the number of terms if `$count` is true,
 *                            otherwise a WP_Error object.
 */
function asfaq_get_categories( $args = array() ) {
	$defaults = array(
		'taxonomy'     => 'as-faq-category',
		'cache_domain' => 'asfaq',
	);

	$args = wp_parse_args( $args, $defaults );

	return get_terms( $args );
}

/**
 * Retrieves the current FAQ category.
 *
 * If on a category archive, that category will be retrieved. Otherwise defaults to the first category.
 *
 * @since 1.1
 *
 * @param bool $query_fallback Whether to use a query fallback if the current category isn't available.
 *                             Default true.
 * @return array|bool|false|mixed|WP_Term Current category, first category if `$query_fallback` is true,
 *                                        otherwise false.
 */
function asfaq_get_current_category( $query_fallback = true ) {
	$category         = get_term_by( 'slug', get_query_var( 'as-faq-category', '' ), 'as-faq-category' );
	$current_category = false;

	if ( $category ) {
		$current_category = $category;
	}

	if ( true === $query_fallback && ! $current_category ) {
		$category_tree = asfaq_get_category_tree( asfaq_get_categories() );

		if ( ! empty( $category_tree[0] ) ) {
			$current_category = $category_tree[0];
		}
	}

	return $current_category;
}

/**
 * Sorts a given list of categories into tree form with full term objects.
 *
 * Note: This function is used recursively.
 *
 * @since 1.1
 *
 * @param \WP_Term[] $categories List of category objects.
 * @param int        $parent     Optional. Parent term ID.
 * @return \WP_Term[] Category tree.
 */
function asfaq_get_category_tree( $categories, $parent = 0 ) {

	$categories = array_map( 'get_term', $categories );

	$category_tree = array();

	foreach ( $categories as $category ) {
		if ( $parent === $category->parent ) {
			$children = asfaq_get_category_tree( $categories, $category->term_id );

			$category->children = asfaq_list_sort( $children, 'name', 'ASC', true );

			$category_tree[] = $category;
		}
	}

	return $category_tree;
}

add_filter( 'term_link', 'asfaq_filter_faq_category_term_link', 10, 3 );
/**
 * Filters term links for FAQ categories when on a collection template.
 *
 * @since 1.1
 *
 * @param string   $link     Term link.
 * @param \WP_Term $term     Term object.
 * @param string   $taxonomy Taxonomy slug.
 * @return string (Maybe) filtered category link.
 */
function asfaq_filter_faq_category_term_link( $link, $term, $taxonomy ) {
	if ( 'as-faq-category' !== $taxonomy ) {
		return $link;
	}

	// Only filter the link if in a collection template.
	if ( asfaq_get_current_collection() ) {
		$link = asfaq_get_category_link_for_collection( $term );
	}

	return $link;
}
