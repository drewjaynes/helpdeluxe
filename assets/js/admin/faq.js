jQuery(document).ready(function ($) {

	$( '#asfaq_slug' ).on( 'keyup', function( event ) {
		$( '.rewrite_slug_preview' ).text( $( this ).val() );
	} );

	$( '#asfaq_collection_slug' ).on( 'keyup', function( event ) {
		$( '.collection_rewrite_slug_preview' ).text( $( this ).val() );
	} );

} );