<?php
/**
 * Template Name: Knowledge Base
 * Template Post Type: as-faq-collection
 */
asfaq_get_header();

if ( 'as-faq-collection' === get_post_type() ) {
	$collection = get_post();
} else {
	$collection = asfaq_get_collection_by_slug( get_query_var( 'collection_name' ) );
}

$titan = TitanFramework::getInstance( 'asfaq_collections' );

$menu_query = array(
	'theme_location'  => 'asfaq-viewer-top-menu',
	'menu_id'         => 'asfaq-viewer-top-menu',
	'container_class' => 'asfaq-viewer-top-menu',
);
?>

<?php if ( $titan->getOption( 'show_title', $collection->ID ) ) : ?>
	<header class="asfaq-kb asfaq-collection-header">
		<h1><?php echo get_the_title( $collection ); ?></h1>
		<?php if ( has_nav_menu( $menu_query['theme_location'] ) ) : ?>
			<span class="asfaq-kb-header-menu">
				<?php wp_nav_menu( $menu_query ); ?>
			</span>
		<?php endif; ?>

	</header>
<?php endif; ?>

<div class="asfaq-collection-content">
	<?php

	$category    = asfaq_get_current_collection_category();
	$faq_item    = asfaq_get_current_collection_faq();
	$categories  = asfaq_get_collection_categories( $collection );

	$show_counts = $titan->getOption( 'item_counts', $collection->ID );
	$layout      = $titan->getOption( 'layout', $collection->ID );

	/*
	 * If the base layout is the knowledge base and this is the landing page,
	 * list out the first 20 FAQs in a bulleted link list instead of trying
	 * to use a nonexistent category.
	 */
	if ( 0 === $category && 'asfaq-collection-knowledge-base.php' === $layout ) {
		$knowledge_base_is_layout = true;
	} else {
		$knowledge_base_is_layout = false;
	}

	$display_args = array(
		'children'    => false,
		'show_header' => false,
	);

	$classes = 'asfaq-kb-main col-3-5';

	if ( $faq_item ) {
		$classes .= " single-faq-{$faq_item}";

		// Attempt to determine the current category based on shared terms between collection and item.
		$item_cats       = wp_get_post_terms( $faq_item, 'as-faq-category', array( 'fields' => 'ids' ) );
		$collection_cats = wp_get_post_terms( $collection->ID, 'as-faq-category', array( 'fields' => 'ids' ) );
		$common_cats     = array_intersect_key( $item_cats, $collection_cats );

		$category = reset( $common_cats );
	}
	?>

	<aside id="asfaq-kb-sidebar" class="col-2-5">
		<ul class="asfaq-kb-sidebar">
			<?php
			$args = array(
				'taxonomy'         => 'as-faq-category',
				'title_li'         => '',
				'current_category' => $category,
				'include'          => $categories,
				'show_count'       => $knowledge_base_is_layout ? $show_counts : false,
			);

			wp_list_categories( $args );
			?>
		</ul>
	</aside>

	<?php $category_obj = get_term( $category ); ?>

	<section id="asfaq-kb-main" class="<?php echo esc_attr( $classes ); ?>">
		<div class="asfaq-kb-header asfaq-breadcrumb">
			<h3>
				<a href="<?php echo esc_attr( get_permalink( $collection ) ); ?>">
					<?php echo get_the_title( $collection ); ?>
				</a>

				<?php if ( ! is_wp_error( $category_obj ) ) : ?>
					 &raquo;
					<a href="<?php echo esc_attr( asfaq_get_category_link_for_collection( $category_obj, $collection ) ); ?>">
						<?php echo esc_html( $category_obj->name ); ?>
					</a>
				<?php endif; ?>
			</h3>
			<hr />
		</div>
		<?php if ( $faq_item ) : ?>
			<h2><?php echo get_the_title( $faq_item ); ?></h2>

			<?php echo apply_filters( 'the_content', get_post_field( 'post_content', $faq_item ) ); ?>

		<?php elseif ( $knowledge_base_is_layout ) : ?>
			<?php
			$faqs = asfaq_get_faqs( array(
				'cache_results'  => true,
				'posts_per_page' => 20,
				'tax_query'      => array(
					array(
						'taxonomy' => 'as-faq-category',
						'fields'   => 'term_id',
						'terms'    => asfaq_get_collection_categories( $collection )
					),
				),
			) );
			?>
			<h4><?php printf( __( 'Topics for %s', 'as-faq' ), get_the_title( $collection ) ); ?></h4>

			<?php if ( ! empty( $faqs ) ) : ?>
				<?php echo asfaq_get_faqs_list_markup( $collection, $faqs, 'links_bullets' ); ?>
			<?php else: ?>
				<?php esc_html_e( 'No FAQ topics were found for this collection.', 'as-faq' ); ?>
			<?php endif; ?>
		<?php else :
			echo asfaq_get_category_faqs_markup( $category_obj, $display_args );
		endif; // Single ?>
	</section>

</div>

<?php asfaq_get_footer(); ?>
