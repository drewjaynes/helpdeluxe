<?php
/**
 * Template Name: Category Lists
 * Template Post Type: as-faq-collection
 */
get_header();

$titan         = TitanFramework::getInstance( 'asfaq_collections' );
$collection    = get_the_ID();
$list_style    = $titan->getOption( 'list_style', $collection );
$show_counts   = $titan->getOption( 'item_counts', $collection );
$category_tree = asfaq_get_category_tree( asfaq_get_collection_categories( $collection ) );
?>

<?php if ( $titan->getOption( 'show_title', $collection ) ) : ?>
	<header class="asfaq-collection-header">
		<h1><?php the_title(); ?></h1>
	</header>
<?php endif; ?>

<div class="asfaq-collection-content collection-grid collection-grid-pad">
	<?php
	if ( empty( $category_tree ) ) {
		// Display "no items" message.
	}

	foreach ( $category_tree as $category ) : ?>

		<div class="col-1-2 as-faq-category-<?php echo esc_attr( $category->term_id ); ?>">

			<?php if ( $category->count > 0 ) : ?>

				<div class="collection-item">

					<a href="<?php echo esc_url( asfaq_get_category_link_for_collection( $category, $collection ) ); ?>">
						<h3>
							<?php if ( $show_counts ) : ?>
								<span class="faq-count"><?php printf( _n( '%s Article', '%s Articles', $category->count, 'asfaq' ), number_format_i18n( $category->count ) ); ?> </span>
							<?php endif; ?>
							<?php echo esc_html( $category->name ); ?>
						</h3>

					</a>

					<?php
					$faqs = asfaq_get_faqs( array(
						'cache_results'  => true,
						'posts_per_page' => 5,
						'tax_query'      => array(
							array(
								'taxonomy' => 'as-faq-category',
								'fields'   => 'term_id',
								'terms'    => $category->term_id
							),
						),
					) );

					echo asfaq_get_faqs_list_markup( $collection, $faqs );
					?>
				</div>
			<?php endif; ?>

		</div>

		<?php if ( ! empty( $category->children ) ) : ?>

			<?php foreach ( $category->children as $child ) : ?>

				<div class="col-1-2 as-faq-category-<?php echo esc_attr( $child->term_id ); ?>">

					<?php if ( $child->count > 0 ) : ?>
						<div class="collection-item">
							<a href="<?php echo esc_url( asfaq_get_category_link_for_collection( $child, $collection ) ); ?>">
								<h3>
									<?php if ( $show_counts ) : ?>
										<span class="faq-count"><?php printf( _n( '%s Article', '%s Articles', $child->count, 'asfaq' ), number_format_i18n( $category->count ) ); ?> </span>
									<?php endif; ?>
									<?php echo esc_html( $child->name ); ?>
								</h3>

							</a>

							<?php
							$faqs = asfaq_get_faqs( array(
								'cache_results'  => true,
								'posts_per_page' => 5,
								'tax_query'      => array(
									array(
										'taxonomy' => 'as-faq-category',
										'fields'   => 'term_id',
										'terms'    => $child->term_id
									),
								),
							) );

							echo asfaq_get_faqs_list_markup( $collection, $faqs );
							?>

						</div>
					<?php endif; ?>

				</div>

			<?php endforeach; ?>

		<?php endif; ?>

	<?php endforeach; ?>
</div>

<?php get_footer(); ?>
