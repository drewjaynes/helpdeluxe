<?php
/**
 * Template Name: Category Boxes
 * Template Post Type: as-faq-collection
 */
get_header();

$titan         = TitanFramework::getInstance( 'asfaq_collections' );
$collection    = get_the_ID();
$list_style    = $titan->getOption( 'list_style', $collection );
$show_counts   = $titan->getOption( 'item_counts', $collection );
$category_tree = asfaq_get_category_tree( asfaq_get_collection_categories( $collection ) );
?>

<?php if ( $titan->getOption( 'show_title', $collection ) ) : ?>
	<header class="asfaq-collection-header">
		<h1><?php the_title(); ?></h1>
	</header>
<?php endif; ?>

<div class="asfaq-collection-content collection-grid collection-grid-pad">
	<?php
	if ( empty( $category_tree ) ) {
		// Display "no items" message.
	}

	foreach ( $category_tree as $category ) : ?>
		<section>
			<div class="grid-section-intro col-1-1">
				<h2 class="grid-section-heading"><?php echo esc_html( $category->name ); ?></h2>
				<p class="grid-section-description">
					<?php echo apply_filters( 'the_content', term_description( $category->term_id, 'as-faq-category' ) ); ?>
				</p>
			</div>

			<?php if ( $category->count > 0 ) : ?>
				<?php $col_class = empty( $category->children ) ? 'col-1-2' : 'col-1-3'; ?>
				<div class="<?php echo esc_attr( $col_class ); ?> category-<?php echo esc_attr( $category->term_id ); ?>">
					<a href="<?php echo esc_url( asfaq_get_category_link_for_collection( $category, $collection ) ); ?>">
						<div class="collection-item">
							<h3 class="grid-box-heading"><?php echo esc_html( $category->name ); ?></h3>

							<p class="grid-box-description"><?php echo esc_attr( $category->description ); ?></p>
							<?php if ( $show_counts ) : ?>
								<p class="grid-box-count">
									<?php printf( _n( '%s Article', '%s Articles', $category->count, 'asfaq' ), number_format_i18n( $category->count ) ); ?>
								</p>
							<?php endif; ?>
						</div>

					</a>
				</div>

			<?php endif; ?>

			<?php if ( ! empty( $category->children ) ) : ?>

				<?php
				// Render children alongside the parent item.
				foreach ( $category->children as $child ) : /** @var \WP_Term $child */ ?>

					<div class="col-1-3 category-<?php echo esc_attr( $child->term_id ); ?>">
						<a href="<?php echo esc_url( asfaq_get_category_link_for_collection( $child, $collection ) ); ?>">
							<div class="collection-item">
								<h3 class="grid-box-heading"><?php echo esc_html( $child->name ); ?></h3>

								<p class="grid-box-description"><?php echo esc_attr( $child->description ); ?></p>
								<?php if ( $show_counts ) : ?>
									<p class="grid-box-count">
										<?php printf( _n( '%s Article', '%s Articles', $child->count, 'asfaq' ), number_format_i18n( $child->count ) ); ?>
									</p>
								<?php endif; ?>
							</div>

						</a>
					</div>

				<?php endforeach; ?>

			<?php endif; ?>

		</section>

	<?php endforeach; ?>
</div>

<?php get_footer(); ?>
