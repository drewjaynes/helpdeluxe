<?php
/**
 * Template Name: Columns
 * Template Post Type: as-faq-collection
 */

get_header();

$titan         = TitanFramework::getInstance( 'asfaq_collections' );
$collection    = get_the_ID();
$list_style    = $titan->getOption( 'list_style', $collection );
$show_counts   = $titan->getOption( 'item_counts', $collection );
?>

<?php if ( $titan->getOption( 'show_title', $collection ) ) : ?>
	<header class="asfaq-collection-header">
		<h1><?php the_title(); ?></h1>
	</header>
<?php endif; ?>

<div class="asfaq-collection-content collection-grid collection-grid-pad">
	<?php
	$faqs = asfaq_get_faqs( array(
		'cache_results'  => true,
		'posts_per_page' => 500,
		'tax_query'      => array(
			array(
				'taxonomy' => 'as-faq-category',
				'fields'   => 'term_id',
				'terms'    => asfaq_get_collection_categories( $collection )
			),
		),
	) );

	// Split columns at 7 items.
	$faq_groups = array_chunk( $faqs, 4 );

	if ( $faq_groups <= 2 ) {
		$class = 'col-1-2';
	} elseif ( $faq_groups >= 3 ) {
		$class = 'col-1-3';
	} else {
		$class = 'col-1-1';
	}

	?>
	<?php foreach ( $faq_groups as $group ) : ?>
		<div class="<?php echo esc_attr( $class ); ?>">
			<div class="collection-item">
				<?php echo asfaq_get_faqs_list_markup( $collection, $group ); ?>
			</div>
		</div>
	<?php endforeach; ?>

</div>

<?php get_footer(); ?>
